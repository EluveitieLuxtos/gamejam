#include "Ennemi.h"
#include "Donjon.h"

using namespace std;

Ennemi::Ennemi(std::string nom,int x, int y)
{
    dir=1;
    img=0;
    t=0;
    //divers variables
    m_vie= 100;
    m_nom= nom;

    velocity=rand()%3+3;

    taillex=21;
    tailley=30;

    //position en pixel sur la fenetre
    m_posx = x*64+(int)((64-taillex)/2);
    m_posy = y*64+(int)((64-tailley)/2);

    //creation du sprite
    sf::Sprite sprite;
    m_sprite = sprite;

    m_col.init_col(m_posx,m_posy,taillex,tailley);

}

void Ennemi::init(int x, int y)
{
    m_posx = x*64+(int)((64-taillex)/2);
    m_posy = y*64+(int)((64-tailley)/2);
    m_col.init_col(m_posx,m_posy,taillex,tailley);
    m_sprite.setPosition(sf::Vector2f(m_posx,m_posy));
}

void Ennemi::init_texture(sf::Texture &t)
{
    m_sprite.setTexture(t);
    m_sprite.setTextureRect(sf::IntRect(0,0,taillex,tailley));
    m_sprite.setPosition(sf::Vector2f(m_posx,m_posy));
}

void Ennemi::recevoirDegats(int degats)
{
    m_vie -= degats;
}

bool Ennemi::estVivant()
{
    return (m_vie>0);
}

Ennemi::~Ennemi()
{

}

int Ennemi::pos_x(){
    return m_posx;
}

int Ennemi::pos_y(){
    return m_posy;
}

sf::Sprite Ennemi::show(){
    return m_sprite;
}

void Ennemi::depl(int x,int y,Donjon niv,bool fight){

    double ox=m_posx;
    double oy=m_posy;

    //deplacement horizontale
    m_sprite.move(x,0);
    m_posx += x;
    //empecher le deplacement si collision sur le cote
    if((detectcoll(niv,fight))){
        m_sprite.move(-x,0);
        m_posx -= x;
    }


    //deplacement verticale
    m_sprite.move(0,y);
    m_posy += y;
    //emempecher le deplacement si collision sur en haut ou en bas
    if((detectcoll(niv,fight))){
        m_sprite.move(0,-y);
        m_posy -= y;
    }


    if((m_posy-oy>0))
    {
        dir=1;
    }
    else if((m_posy-oy<0))
        dir=3;

    if((m_posx-ox>0))
    {
        dir=0;
    }
    else if ((m_posx-ox<0))
    {
        dir=2;
    }

    //image
    t=(t+1)%8;
    if(t==0)
    {
        if ((m_posx-ox!=0 or m_posy-oy!=0 ))
            img = (img+1)%4;
        else
            img=1;
    }

    m_sprite.setTextureRect(sf::IntRect(taillex*img,tailley*dir,taillex,tailley));

    //collider
    m_col.chg_pos(m_posx-m_col.positionx(),m_posy-m_col.positiony());

}

bool Ennemi::detectcoll(Donjon niv,bool fight){
    bool col=false;

    int x=(int)m_posx/64;
    int y=(int)m_posy/64;
    int hy=(int)(m_posy+15)/64;

    int dx=m_posx%64;
    int dy=m_posy%64;

    col=(x<0)||(y<0)||
        (niv.structure()[x][hy]==MUR)||
        (dy>64-tailley && niv.structure()[x][y+1]==MUR)||
        (dx>64-taillex && niv.structure()[x+1][hy]==MUR)||
        (dx>64-taillex && dy>64-tailley && niv.structure()[x+1][y+1]==MUR);


    return col;
}

bool Ennemi::detectplayer(int xperso, int yperso, int difficulty){

    int dx = m_posx - xperso;
    int dy = m_posy - yperso;

    int dist_square = dx*dx + dy*dy;

    return dist_square < (difficulty*64)*(difficulty*64)*4;
}

int Ennemi::dist_play(int xperso, int yperso)
{
    int dx = m_posx - xperso;
    int dy = m_posy - yperso;

    return dx*dx + dy*dy;
}

void Ennemi::depltoplayer(Donjon niv, int xperso, int yperso, int difficulty){

    bool detected = detectplayer(xperso, yperso, difficulty);

    if (detected)
    {
        double alpha = atan2((double)(yperso - m_posy),(double)(xperso - m_posx));
        int dx = (int) ((1+difficulty/10)*velocity*cos(alpha));
        int dy = (int) (velocity*sin(alpha));
        depl(dx, dy, niv, true);
   }
}
