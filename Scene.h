#ifndef SCENE_H
#define SCENE_H
#include<windows.h>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#ifndef WIN32
    #include <sys/types.h>
#endif

#include <map>

#include "Perso.h"
#include "Donjon.h"
#include "Ennemi.h"


#define deltacase 6
#define SPEED 4
#define H_W 600
#define L_W 900


void updateperso(Personnage *perso1, Donjon *niv);

typedef struct
{
    int bois;
    int pierre;
    int fer;
    int pp;
    int argent;
    int spe_bois;
    int spe_pierre;
    int spe_fer;
    int spe_pp;
    int spe_argent;
    bool continuer;
} total;

class Scene
{
    public:
        Scene();
        virtual ~Scene();


        bool Village(sf::RenderWindow&,Personnage*,int, int);
        bool Village2(sf::RenderWindow &window, Personnage *perso1,int djnum,int nivbug, int pack);


    protected:
    private:
};

#endif // SCENE_H
