#include "Collider2D.h"

Collider2Dc::Collider2Dc()
{
    //ctor
}

Collider2Dc::~Collider2Dc()
{
    //dtor
}


bool Collider2Dc::coll_collide(Collider2Dc coll2)
{
    bool a1, a2, a3, a4;

    int Px1 = this->posx ;
    int Py1 = this->posy ;
    int Px2 = this->posx + cos(this->angle)*this->width;
    int Py2 = this->posy + sin(this->angle)*this->width;
    int Px3 = this->posx + cos(this->angle)*this->width +sin(this->angle-PI/2)*this->height;
    int Py3 = this->posy + sin(this->angle)*this->width +cos(this->angle-PI/2)*this->height;
    int Px4 = this->posx + cos(this->angle-PI/2)*this->height;
    int Py4 = this->posy + sin(this->angle-PI/2)*this->height;

    int Tx1 = coll2.posx;
    int Ty1 = coll2.posy;
    int Tx2 = coll2.posx+ cos(coll2.angle)*this->width;
    int Ty2 = coll2.posy+ sin(coll2.angle)*this->width;
    int Tx3 = coll2.posx+ cos(coll2.angle)*this->width +sin(coll2.angle-PI/2)*this->height;
    int Ty3 = coll2.posy+ sin(coll2.angle)*this->width +cos(coll2.angle-PI/2)*this->height;
    int Tx4 = coll2.posx+ cos(coll2.angle-PI/2)*this->height;
    int Ty4 = coll2.posy+ sin(coll2.angle-PI/2)*this->height;

    a1= (((Px2-Px1)*(Ty1-Py1)-(Py2-Py1)*(Tx1-Px1)<=0)
         && ((Px3-Px2)*(Ty1-Py2)-(Py3-Py2)*(Tx1-Px2)<=0)
         && ((Px4-Px3)*(Ty1-Py3)-(Py4-Py3)*(Tx1-Px3)<=0)
         && ((Px1-Px4)*(Ty1-Py4)-(Py1-Py4)*(Tx1-Px4)<=0));
    a2= (((Px2-Px1)*(Ty2-Py1)-(Py2-Py1)*(Tx2-Px1)<=0)
         && ((Px3-Px2)*(Ty2-Py2)-(Py3-Py2)*(Tx2-Px2)<=0)
         && ((Px4-Px3)*(Ty2-Py3)-(Py4-Py3)*(Tx2-Px3)<=0)
         && ((Px1-Px4)*(Ty2-Py4)-(Py1-Py4)*(Tx2-Px4)<=0));
    a3= (((Px2-Px1)*(Ty3-Py1)-(Py2-Py1)*(Tx3-Px1)<=0)
         && ((Px3-Px2)*(Ty3-Py2)-(Py3-Py2)*(Tx3-Px2)<=0)
         && ((Px4-Px3)*(Ty3-Py3)-(Py4-Py3)*(Tx3-Px3)<=0)
         && ((Px1-Px4)*(Ty3-Py4)-(Py1-Py4)*(Tx3-Px4)<=0));
    a4= (((Px2-Px1)*(Ty4-Py1)-(Py2-Py1)*(Tx4-Px1)<=0)
         && ((Px3-Px2)*(Ty4-Py2)-(Py3-Py2)*(Tx4-Px2)<=0)
         && ((Px4-Px3)*(Ty4-Py3)-(Py4-Py3)*(Tx4-Px3)<=0)
         && ((Px1-Px4)*(Ty4-Py4)-(Py1-Py4)*(Tx4-Px4)<=0));

    return a1||a2||a3||a4;
}

bool Collider2Dc::coll_groupe(vector<Collider2Dc> gc2d)
{
    unsigned int i=0;
    bool col=false;

    while (i<gc2d.size() && not(col))
    {
        col=coll_collide(gc2d[i]);
        i++;
    }

    return col;
}

void Collider2Dc::coll_rotate(double ang)
{
    angle+=ang;
}

void Collider2Dc::chg_pos(int x, int y)
{
    posx+=x;
    posy+=y;
}

void Collider2Dc::chg_taille(int w, int h)
{
    width=w;
    height=h;
}

double Collider2Dc::coll_rotation()
{
    return angle;
}

int Collider2Dc::positionx()
{
    return (posx);
}

int Collider2Dc::positiony()
{
    return (posy);
}

int Collider2Dc::taillew()
{
    return width;
}

int Collider2Dc::tailleh()
{
    return height;
}

void Collider2Dc::init_col(int x, int y, int w, int h)
{
    posx=x;
    posy=y;
    width=w;
    height=h;
}
