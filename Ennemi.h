#ifndef ENNEMI_H
#define ENNEMI_H

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Donjon.h"

#include <string>

typedef struct{
    int x;
    int y;
} coord;

class Donjon;

class Ennemi
{
    public:
        Ennemi(std::string nom,int x,int y);
        ~Ennemi();
        void init(int x,int y);
        void recevoirDegats(int degats);
        bool estVivant();
        int pos_y();
        int pos_x();
        sf::Sprite show();
        void depl(int x, int y,Donjon niv,bool fight);
        bool detectcoll(Donjon niv,bool fight);
        bool detectplayer(int xperso, int yperso, int difficulty);
        void locate(int x, int y,Donjon niv);
        void depltoplayer(Donjon niv, int xperso, int yperso, int difficulty);
        void init_texture(sf::Texture &t);
        int dist_play(int xperso, int yperso);

        int velocity;

        Collider2Dc m_col;

    //private:
    protected: //Priv�, mais accessible aux �l�ments enfants
        vector<coord> trace;
        int m_vie;
        std::string m_nom;
        int m_posx;
        int m_posy;
        sf::Sprite m_sprite;


        int taillex;
        int tailley;

        int dir;
        int img;
        int t;

};

#endif // ENNEMI_H
