#include "Perso.h"

using namespace std;

Personnage::Personnage(std::string nom,sf::Texture& texture)
{
    t=0;
    dir=1;
    img=0;
    //divers variables
    m_vie= rand()%6+5;
    m_nom= nom;

    //position en pixel sur la fenetre
    m_posx = 0;
    m_posy = 0;

    taillex=21;
    tailley=30;

    //creation du sprite
    sf::Sprite sprite;
    m_sprite = sprite;
    m_sprite.setTexture(texture);
    m_sprite.setTextureRect(sf::IntRect(0,0,taillex,tailley));
    m_sprite.setPosition(sf::Vector2f(m_posx,m_posy));

    m_col.init_col(m_posx,m_posy,taillex,tailley);

}

void Personnage::initpos(int x, int y)
{
    m_posx=x*64 +(64-taillex)/2;
    m_posy=y*64 +(64-tailley)/2;
    m_sprite.setPosition(sf::Vector2f(m_posx,m_posy));

    m_col.init_col(m_posx,m_posy,taillex,tailley);
}

void Personnage::recevoirDegats(int degats)
{
    m_vie -= degats;
}

bool Personnage::estVivant()
{
    return (m_vie>0);
}

Personnage::~Personnage()
{

}

int Personnage::pos_x(){
    return m_posx;
}

int Personnage::pos_y(){
    return m_posy;
}

sf::Sprite Personnage::show(){
    return m_sprite;
}

void Personnage::depl(int x,int y,Donjon niv){

    double ox=m_posx;
    double oy=m_posy;

    //deplacement horizontale
    m_sprite.move(x,0);
    m_posx += x;
    //empecher le deplacement si collision sur le cote
    if((detectcoll(niv,false))){
        m_sprite.move(-x,0);
        m_posx -= x;
    }




    //deplacement verticale
    m_sprite.move(0,y);
    m_posy += y;
    //emempecher le deplacement si collision sur en haut ou en bas
    if((detectcoll(niv,false))){
        m_sprite.move(0,-y);
        m_posy -= y;
    }

    t=(t+1)%4;

    if((m_posy-oy>0))
        dir=1;
    else if((m_posy-oy<0))
        dir=3;

    if((m_posx-ox>0))
        dir=0;
    else if ((m_posx-ox<0))
        dir=2;

    //image
    if (t==0)
    {
        if ((m_posx-ox!=0 or m_posy-oy!=0 ))
            img = (img+1)%4;
        else
            img=1;
    }

    m_sprite.setTextureRect(sf::IntRect(taillex*img,tailley*dir,taillex,tailley));

    //collider
    m_col.chg_pos(m_posx-m_col.positionx(),m_posy-m_col.positiony());

}

bool Personnage::detectcoll(Donjon niv,bool fight){
    bool col=false;

    int x=(int)m_posx/64;
    int y=(int)m_posy/64;
    int hy=(int)(m_posy+15)/64;

    int dx=m_posx%64;
    int dy=m_posy%64;

    col=(x<0)||(y<0)||
        (niv.structure()[x][hy]==MUR)||
        (dy>64-tailley && niv.structure()[x][y+1]==MUR)||
        (dx>64-taillex && niv.structure()[x+1][hy]==MUR)||
        (dx>64-taillex && dy>64-tailley && niv.structure()[x+1][y+1]==MUR);


    return col;
}

bool Personnage::detect_sortie(Donjon niv){
    bool col=false;

    int x=(int)m_posx/64;
    int y=(int)m_posy/64;

    int dx=m_posx%64;
    int dy=m_posy%64;

    col=(niv.structure()[x][y]==SORTIE) && (dy<64-tailley)&& (dx<64-taillex);


    return col;
}

