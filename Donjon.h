#ifndef DONJON_H
#define DONJON_H

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <string>

#include "Collider2D.h"
#include "Ennemi.h"
#include "Perso.h"

using namespace std;

class Personnage;

typedef enum
{
    VIDE,
    AIR,
    MUR,
    SORTIE,
    ENTREE,
    MOBSPAWNER,
}Type_Bloc;

class Ennemi;

class Donjon
{
    public:
        Donjon(string nom_salle="");
        virtual ~Donjon();
        void GenDonjon(string nom_salle);
        vector<vector<int> > structure();
        vector<vector<sf::Sprite> > Sprite_struct();
        vector<Ennemi> en_donj;
        void generer_ennemi();
        void renew(Personnage *p);
        int d_x, d_y;

    protected:
    private:
        vector<vector<int> > m_structure;
        vector<vector<sf::Sprite> > Spr_str;

};

#endif // DONJON_H
