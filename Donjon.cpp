#include "Donjon.h"


Donjon::Donjon(string nom_salle)
{
    //ctor
    GenDonjon(nom_salle);
}

Donjon::~Donjon()
{
    //dtor
    for (unsigned int k=0;k<Spr_str.size();k++)
        Spr_str[k].clear();
    Spr_str.clear();
    for (unsigned int k=0;k<m_structure.size();k++)
        m_structure[k].clear();
    m_structure.clear();
}

vector<vector<sf::Sprite> > Donjon::Sprite_struct()
{
    return Spr_str;
}

vector<vector<int> > Donjon::structure()
{
    return m_structure;
}

void Donjon::GenDonjon(string nom_fichier)
{

    vector<int> enne;
    int nb_enn=0;
    string line, code_cour("");

    int code_bloc; //Les 3 infos a collecter pour chaque bloc

    ifstream fic(nom_fichier.c_str(), ios::in);
    if(fic)
    {
        int i=0;
        while(getline(fic, line))
        {
            vector<int> v;
            m_structure.push_back(v);
            istringstream iss(line);
            while(!iss.eof()) //Tant qu'on a pas lu tout le flux
            {
                iss >> code_bloc; //On recupere les infos
                m_structure[i].push_back(code_bloc);
                if (code_bloc==ENTREE)
                {
                    d_y=m_structure[i].size()-1;
                    d_x=m_structure.size()-1;
                }
                else if (code_bloc==MOBSPAWNER)
                {
                    enne.push_back(m_structure[i].size()-1);
                    enne.push_back(m_structure.size()-1);
                    nb_enn++;
                }
            }
            i++;
        }
        fic.close();
    }
    else
    {
        cerr << "Impossible d'ouvrir le fichier " << nom_fichier << endl;
    }
    for(int i=0;i<m_structure.size();i++)
        reverse(m_structure[i].begin(),m_structure[i].end());
    reverse(m_structure.begin(),m_structure.end());
    d_y=m_structure[0].size()-1-d_y;
    d_x=m_structure.size()-1-d_x;
    for(int i=0; i<nb_enn;i++)
    {
        Ennemi e("",m_structure.size()-1-enne[i*2+1],m_structure[0].size()-1-enne[i*2]);
        en_donj.push_back(e);
    }
}


void Donjon::renew(Personnage* p)
{
    int k=0;
    for(int i=0; i<m_structure.size();i++)
    {
        for(int j=0; j<m_structure[i].size();j++)
        {
            if(m_structure[i][j]==MOBSPAWNER)
            {
                en_donj[k].init(i,j);
            }
            if(m_structure[i][j]==ENTREE)
            {
                p->initpos(d_x,d_y);
            }
        }
    }
}
