#ifndef PERSO_H_INCLUDED
#define PERSO_H_INCLUDED

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Donjon.h"

#include <string>
class Donjon;

class Personnage
{
    public:
        Personnage(std::string nom, sf::Texture& texture);
        ~Personnage();
        void recevoirDegats(int degats);
        void initpos(int,int);
        bool estVivant();
        int pos_y();
        int pos_x();
        sf::Sprite show();
        void depl(int x, int y,Donjon niv);
        bool detectcoll(Donjon niv,bool fight);
        bool detect_sortie(Donjon niv);
        int m_vie;
        Collider2Dc m_col;

    //private:
    protected: //Priv�, mais accessible aux �l�ments enfants

        std::string m_nom;
        int m_posx;
        int m_posy;
        sf::Sprite m_sprite;
        int t;

        int taillex;
        int tailley;

        int dir;
        int img;

};

#endif // PERSO_H_INCLUDED
