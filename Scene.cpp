#include "Scene.h"

Scene::Scene()
{
    //ctor
}

Scene::~Scene()
{
    //dtor
}


bool Scene::Village(sf::RenderWindow &window, Personnage *perso1,int djnum, int seed)
{
    //definition du dico des textures
    map<string, sf::Texture> decor; // dictionnaire des textures
    map<string, sf::Texture> decorsol; // dictionnaire des textures
    map<string, sf::Texture> decormur; // dictionnaire des textures
    string nom;

    DIR *rep;
    struct dirent *lecture;

    sf::Sprite fond;
    sf::Texture textfond;

    int ts=0,tm=0,tb=0;

    int pack=1+rand()%4;
    stringstream s_rep;
    s_rep << ".\\image\\pack"<<pack<<"\\";
    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        s_rep.str("");
        s_rep << ".\\image\\pack"<<pack<<"\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        decor.insert(make_pair(nom,dtexture));}
    }
    closedir(rep);
    s_rep.str("");
    s_rep << ".\\image\\pack"<<pack<<"\\sols\\";

    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        s_rep.str("");
        s_rep << ".\\image\\pack"<<pack<<"\\sols\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        decorsol.insert(make_pair(nom,dtexture));ts++;}
    }
    closedir(rep);

    s_rep.str("");
    s_rep << ".\\image\\pack"<<pack<<"\\murs\\";

    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        s_rep.str("");
        s_rep << ".\\image\\pack"<<pack<<"\\murs\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        decormur.insert(make_pair(nom,dtexture));tm++;}
    }
    closedir(rep);


    char sniv[25];
    if(djnum!=14)
        sprintf(sniv,".\\Salles\\salle%d.txt",djnum);
    else
        sprintf(sniv,".\\Salles\\salleBoss.txt");




    Donjon niv1(sniv);

    perso1->initpos(niv1.d_x,niv1.d_y);

    int nivtaillex=niv1.structure().size();
    int nivtailley=niv1.structure()[0].size();

    sf::Texture enntexture;
    vector<sf::Texture> enn_v_tex;
    char s_e[25];
    s_rep.str("");
    s_rep << ".\\image\\pack"<<pack<<"\\mobs\\";
    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        s_rep.str("");
        s_rep << ".\\image\\pack"<<pack<<"\\mobs\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        enn_v_tex.push_back(dtexture);}
    }
    closedir(rep);

    for(int j=0;j<niv1.en_donj.size();j++)
    {
        niv1.en_donj[j].init_texture(enn_v_tex[rand()%enn_v_tex.size()]);
    }

    sf::Sprite sprite;
    sf::Sprite sprite2;
    sprite.setTextureRect(sf::IntRect(0,0,64,64));
    sprite2.setTextureRect(sf::IntRect(0,0,64,64));

    sf::RenderTexture render,renderp;
    render.create(nivtaillex*64, nivtailley*64);
    renderp.create(nivtaillex*64, nivtailley*64);
    char s[6];
    for (int i=0;i<nivtaillex;i++){
        for (int j=0;j<nivtailley;j++){
            switch (niv1.structure()[i][j]){
            case AIR:
                sprintf(s,"sol");
                sprite.setTexture(decor[s],true);
                sprite.setPosition(i*64,j*64);
                render.draw(sprite);
                if (((float)rand())/RAND_MAX<0.125)
                {
                    sprintf(s,"%d",rand()%ts);
                    sprite2.setTexture(decorsol[s],true);
                    sprite2.setPosition(i*64,j*64);
                    render.draw(sprite2);
                }
                break;
            case MUR:
                sprintf(s,"mur");
                sprite.setTexture(decor[s],true);
                sprite.setPosition(i*64,j*64);
                render.draw(sprite);
                if (((float)rand())/RAND_MAX<0.125)
                {
                    sprintf(s,"%d",rand()%tm);
                    sprite2.setTexture(decormur[s],true);
                    sprite2.setPosition(i*64,j*64);
                    render.draw(sprite2);
                }
                break;
            case SORTIE:
                sprintf(s,"sortie");
                sprite.setTexture(decor[s],true);
                sprite.setPosition(i*64,j*64);
                render.draw(sprite);
                break;
            case ENTREE:
            case MOBSPAWNER:
                sprintf(s,"sol");
                sprite.setTexture(decor[s],true);
                sprite.setPosition(i*64,j*64);
                render.draw(sprite);
                break;
            default:
                sprintf(s,"void");
                sprite.setTexture(decor[s],true);
                sprite.setPosition(i*64,j*64);
                render.draw(sprite);
                break;
            }
        }
    }
    render.display();
    sf::Sprite spr_niv(render.getTexture());
    spr_niv.setPosition(0,0);

    renderp.display();
    sf::Sprite porte_niv(renderp.getTexture());
    porte_niv.setPosition(0,0);


//no
    sf::Texture Notext;
    if (!Notext.loadFromFile(".\\image\\No1.png")){}
    sf::Sprite Nosp; Nosp.setTexture(Notext,true);
    Nosp.setTextureRect(sf::IntRect(0,0,900,600));
    Notext.setRepeated(true);
    Nosp.setPosition(0,0);


    if (!textfond.loadFromFile(".\\image\\FondNoir.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,64*nivtaillex,64*nivtailley));
    textfond.setRepeated(true);

    fond.setPosition(0,0);


    //synchronisation verticale
    window.setVerticalSyncEnabled(true);

    //setting du view
    sf::View view1;
    view1.setCenter(sf::Vector2f(perso1->pos_x(), perso1->pos_y()));
    view1.setSize(sf::Vector2f(2*deltacase*64+64, deltacase*64*2*2/3+64));
    window.setView(view1);

    bool sortie=false;
    bool touche=false;

    while (window.isOpen()&& not(sortie) && perso1->m_vie!=0)
    {
        if (touche)
        {
            niv1.renew(perso1);
            touche=false;
        }
        window.clear();
        // on inspecte tous les �v�nements de la fen�tre qui ont �t� �mis depuis la pr�c�dente it�ration
        sf::Event event;
        while (window.pollEvent(event))
        {
            // �v�nement "fermeture demand�e" : on ferme la fen�tre
            if (event.type == sf::Event::Closed){
                window.close();}
        }

        updateperso(perso1,&niv1);
        for(int j=0;j<niv1.en_donj.size();j++)
        {
            niv1.en_donj[j].depltoplayer(niv1, perso1->pos_x(), perso1->pos_y(), 1);
            if (niv1.en_donj[j].dist_play(perso1->pos_x(), perso1->pos_y())<20)
            {
                touche=true;
                perso1->m_vie--;
            }
        }
        sortie=perso1->detect_sortie(niv1);
        //si camera centrer sur le personnage
        view1.setCenter(sf::Vector2f(perso1->pos_x()+21/2, perso1->pos_y()+30/2));
        window.setView(view1);


        window.draw(fond);
        window.draw(spr_niv);
        window.draw(porte_niv);
        window.draw(perso1->show());
        for(int j=0;j<niv1.en_donj.size();j++)
        {
            window.draw(niv1.en_donj[j].show());
        }

        window.display();

    }
    
    if (sortie)
        return false;
    else
        return true;
}

bool Scene::Village2(sf::RenderWindow &window, Personnage *perso1,int djnum,int nivbug, int pack)
{
    //definition du dico des textures
    map<string, sf::Texture> decor; // dictionnaire des textures
    map<string, sf::Texture> decorsol; // dictionnaire des textures
    map<string, sf::Texture> decormur; // dictionnaire des textures
    map<string, sf::Texture> decorbug; // dictionnaire des textures
    string nom;
    DIR *rep;
    struct dirent *lecture;

    sf::Sprite fond;
    sf::Texture textfond;

    int ts=0,tm=0,tb=0;

    stringstream s_rep;
    s_rep << ".\\image\\pack"<<pack<<"\\";

    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        s_rep.str("");
        s_rep<< ".\\image\\pack"<<pack<<"\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        decor.insert(make_pair(nom,dtexture));}
    }
    closedir(rep);

    rep = opendir(".\\image\\bug\\");
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        if (!dtexture.loadFromFile(".\\image\\bug\\"+nom+".png")){}
        decorbug.insert(make_pair(nom,dtexture));tb++;}
    }
    closedir(rep);

    s_rep.str("");
    s_rep<< ".\\image\\pack"<<pack<<"\\sols\\";
    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        s_rep.str("");
        s_rep<< ".\\image\\pack"<<pack<<"\\sols\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        decorsol.insert(make_pair(nom,dtexture));ts++;}
    }
    closedir(rep);

    s_rep.str("");
    s_rep<< ".\\image\\pack"<<pack<<"\\murs\\";
    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;
        s_rep.str("");
        s_rep<< ".\\image\\pack"<<pack<<"\\murs\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        decormur.insert(make_pair(nom,dtexture));tm++;}
    }
    closedir(rep);


    char sniv[25];
    if(djnum!=14)
        sprintf(sniv,".\\Salles\\salle%d.txt",djnum);
    else
        sprintf(sniv,".\\Salles\\salleBoss.txt");




    Donjon niv1(sniv);
    if (djnum==14)
    {
        niv1.en_donj[0].velocity=5;
    }

    perso1->initpos(niv1.d_x,niv1.d_y);

    int nivtaillex=niv1.structure().size();
    int nivtailley=niv1.structure()[0].size();

    sf::Texture enntexture;
    vector<sf::Texture> enn_v_tex;
    char s_e[25];
    s_rep.str("");
    s_rep<< ".\\image\\pack"<<pack<<"\\mobs\\";
    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".png")!=0)){}
        else{
        nom = nom.substr(0,i-4);
        sf::Texture dtexture;s_rep.str("");
        s_rep<< ".\\image\\pack"<<pack<<"\\mobs\\"<<nom<<".png";
        if (!dtexture.loadFromFile(s_rep.str().c_str())){}
        enn_v_tex.push_back(dtexture);}
    }
    closedir(rep);

    for(int j=0;j<niv1.en_donj.size();j++)
    {
        niv1.en_donj[j].init_texture(enn_v_tex[rand()%enn_v_tex.size()]);
    }

    sf::Sprite sprite;
    sf::Sprite sprite2;
    sprite.setTextureRect(sf::IntRect(0,0,64,64));
    sprite2.setTextureRect(sf::IntRect(0,0,64,64));

    sf::RenderTexture render,renderp;
    render.create(nivtaillex*64, nivtailley*64);
    renderp.create(nivtaillex*64, nivtailley*64);
    char s[6];
    for (int i=0;i<nivtaillex;i++){
        for (int j=0;j<nivtailley;j++){
            if (((float)rand())/RAND_MAX<0.1*nivbug)
              { sprintf(s,"%d",rand()%tb);
                sprite.setTexture(decorbug[s],true);
                sprite.setPosition(i*64,j*64);
                render.draw(sprite);}
            else
            {
                switch (niv1.structure()[i][j]){
                case AIR:
                    sprintf(s,"sol");
                    sprite.setTexture(decor[s],true);
                    sprite.setPosition(i*64,j*64);
                    render.draw(sprite);
                    if (((float)rand())/RAND_MAX<0.125)
                    {
                        sprintf(s,"%d",rand()%ts);
                        sprite2.setTexture(decorsol[s],true);
                        sprite2.setPosition(i*64,j*64);
                        render.draw(sprite2);
                    }
                    break;
                case MUR:
                    sprintf(s,"mur");
                    sprite.setTexture(decor[s],true);
                    sprite.setPosition(i*64,j*64);
                    render.draw(sprite);
                    if (((float)rand())/RAND_MAX<0.125)
                    {
                        sprintf(s,"%d",rand()%tm);
                        sprite2.setTexture(decormur[s],true);
                        sprite2.setPosition(i*64,j*64);
                        render.draw(sprite2);
                    }
                    break;
                case SORTIE:
                    sprintf(s,"sortie");
                    sprite.setTexture(decor[s],true);
                    sprite.setPosition(i*64,j*64);
                    render.draw(sprite);
                    break;
                case ENTREE:
                case MOBSPAWNER:
                    sprintf(s,"sol");
                    sprite.setTexture(decor[s],true);
                    sprite.setPosition(i*64,j*64);
                    render.draw(sprite);
                    break;
                default:
                    sprintf(s,"void");
                    sprite.setTexture(decor[s],true);
                    sprite.setPosition(i*64,j*64);
                    render.draw(sprite);
                    break;
                }
            }
        }
    }
    render.display();
    sf::Sprite spr_niv(render.getTexture());
    spr_niv.setPosition(0,0);

    renderp.display();
    sf::Sprite porte_niv(renderp.getTexture());
    porte_niv.setPosition(0,0);


//no
    sf::Texture Notext;
    if (!Notext.loadFromFile(".\\image\\No1.png")){}
    sf::Sprite Nosp; Nosp.setTexture(Notext,true);
    Nosp.setTextureRect(sf::IntRect(0,0,900,600));
    Notext.setRepeated(true);
    Nosp.setPosition(0,0);


    if (!textfond.loadFromFile(".\\image\\FondNoir.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,64*nivtaillex,64*nivtailley));
    textfond.setRepeated(true);

    fond.setPosition(0,0);


    //synchronisation verticale
    window.setVerticalSyncEnabled(true);

    //setting du view
    sf::View view1;
    view1.setCenter(sf::Vector2f(perso1->pos_x(), perso1->pos_y()));
    view1.setSize(sf::Vector2f(2*deltacase*64+64, deltacase*64*2*2/3+64));
    window.setView(view1);

    bool sortie=false;
    bool touche=false;

    while (window.isOpen()&& not(sortie) && perso1->m_vie!=0)
    {
        if (touche)
        {
            niv1.renew(perso1);
            touche=false;
        }
        window.clear();
        // on inspecte tous les �v�nements de la fen�tre qui ont �t� �mis depuis la pr�c�dente it�ration
        sf::Event event;
        while (window.pollEvent(event))
        {
            // �v�nement "fermeture demand�e" : on ferme la fen�tre
            if (event.type == sf::Event::Closed){
                window.close();}
        }

        updateperso(perso1,&niv1);
        for(int j=0;j<niv1.en_donj.size();j++)
        {
            if (djnum!=14)
                niv1.en_donj[j].depltoplayer(niv1, perso1->pos_x(), perso1->pos_y(), nivbug);
            else
                niv1.en_donj[j].depltoplayer(niv1, perso1->pos_x(), perso1->pos_y(), 5);
            if (niv1.en_donj[j].dist_play(perso1->pos_x(), perso1->pos_y())<20)
            {
                touche=true;
                perso1->m_vie--;
            }
        }
        sortie=perso1->detect_sortie(niv1);
        //si camera centrer sur le personnage
        view1.setCenter(sf::Vector2f(perso1->pos_x()+21/2, perso1->pos_y()+30/2));
        window.setView(view1);


        if (not(sortie))
        {
            window.draw(fond);
            window.draw(spr_niv);
            window.draw(porte_niv);
            window.draw(perso1->show());
            for(int j=0;j<niv1.en_donj.size();j++)
            {
                window.draw(niv1.en_donj[j].show());
            }

            window.display();
        }
        else
        {
            for(int k=0;k<50;k++)
            {
                view1.setCenter(sf::Vector2f(450,300));
                view1.setSize(sf::Vector2f(900, 600));
                window.setView(view1);
                window.clear();
                int r=rand()%5;
                window.draw(fond);
                window.draw(Nosp);
                for(int j=0;j<5*nivbug+r;j++)
                {
                    sprintf(s,"%d",rand()%tb);
                    sprite.setTexture(decorbug[s],true);
                    sprite.setPosition(rand()%836,rand()%536);
                    window.draw(sprite);
                }

                window.display();
            }
            window.draw(fond);
            window.display();
        }

    }

    if (sortie)
        return false;
    else
        return true;
}


