#define _WIN32_WINNT 0x0601
#include<windows.h>
typedef struct _CONSOLE_FONT_INFOEX
{
    ULONG cbSize;
    DWORD nFont;
    COORD dwFontSize;
    UINT  FontFamily;
    UINT  FontWeight;
    WCHAR FaceName[LF_FACESIZE];
}CONSOLE_FONT_INFOEX, *PCONSOLE_FONT_INFOEX;
//the function declaration begins
#ifdef __cplusplus
extern "C" {
#endif
BOOL WINAPI SetCurrentConsoleFontEx(HANDLE hConsoleOutput, BOOL bMaximumWindow, PCONSOLE_FONT_INFOEX
lpConsoleCurrentFontEx);
#ifdef __cplusplus
}
#endif

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#ifndef WIN32
    #include <sys/types.h>
#endif

#include <map>

#include "Perso.h"
#include "Donjon.h"
#include "Ennemi.h"
#include "Scene.h"

#define deltacase 6
#define SPEED 4
#define H_W 600
#define L_W 900

#include <cwchar>
#include <conio.h>


void updateperso(Personnage *perso1, Donjon *niv)
{
    int x(0),y(0);
    //bouger le perso vers la droite et la gauche:
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){x=SPEED;}
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){x=-SPEED;}
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){y=-SPEED;}
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){y=SPEED;}

    //deplacement du joueur
    perso1->depl(x,y,*niv);
}

void consol_dimish()
{
    CONSOLE_FONT_INFOEX cfi;
    cfi.cbSize = sizeof(cfi);
    cfi.nFont = 0;
    cfi.dwFontSize.X = 0;                   // Width of each character in the font
    cfi.dwFontSize.Y = 1;                  // Height
    cfi.FontFamily = FF_DONTCARE;
    cfi.FontWeight = FW_NORMAL;
    std::wcscpy(cfi.FaceName, L"Consolas"); // Choose your font
    SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
}

void SetWindow()
{
    HWND consoleWindow = GetConsoleWindow();

	SetWindowPos( consoleWindow, 0, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
    system("mode 400");

}

void afficher_ennemi_c(string nom_fichier)
{
    string line;

    ifstream fic(nom_fichier.c_str(), ios::in);
    if(fic)
    {
        int i=0;
        while(getline(fic, line))
        {
            cout<<line<<endl;
        }
        fic.close();
    }
}

int main()
{
    sf::Music music;
    //console layers
    consol_dimish();
    SetWindow();

    sf::View view1;
    sf::Sprite fond;
    sf::Texture textfond;
    if (!textfond.loadFromFile(".\\image\\FondNoir.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);

    fond.setPosition(0,0);

    //creation personnage
    sf::Texture ptexture;
    if (!ptexture.loadFromFile(".\\image\\Perso.png")){}
    Personnage perso1("Garry",ptexture);
     //creation fenetre
    sf::RenderWindow window(sf::VideoMode(L_W, H_W), "Abscond-the game");


    //random init
    int seed=0;
    int pack=0;
    stringstream s_rep;
    DIR *rep;
    struct dirent *lecture;
    string nom;
    unsigned int genseed;

    if(not(seed))
        genseed=time(NULL);
    else
        genseed= seed;
    srand(genseed);

    // creation et jouage de la 1ere scene
    Scene scene1;

    int nbmap=rand()%8+3;
    vector<int> possible;
    for(int k=1;k<14;k++)
        possible.push_back(k);
    int salle=1,isalle;
    bool arret=false;
    view1.setCenter(sf::Vector2f(800,600));
    view1.setSize(sf::Vector2f(1600,1200));
    window.setView(view1);
    if (!textfond.loadFromFile(".\\image\\Intro.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,1600,1200));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();

    Sleep(3000);

    for (int k=0;k<nbmap;k++)
    {
        isalle=rand()%possible.size();
        salle=possible[rand()%possible.size()];
        possible.erase(possible.begin()+isalle);

        pack=1+rand()%4;
        vector<string> music_list;
        s_rep.str("");
        s_rep << ".\\music\\bugged\\pack"<<pack<<"\\";
        rep = opendir(s_rep.str().c_str());
        while ((lecture = readdir(rep))){
            nom = (lecture->d_name);
            int i = nom.size();
            if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".wav")!=0)){}
            else{
                nom = nom.substr(0,i-4);
                s_rep.str("");
                s_rep << ".\\music\\bugged\\pack"<<pack<<"\\"<<nom<<".wav";
                music_list.push_back(s_rep.str().c_str());}
        }
        closedir(rep);
        if (!music.openFromFile(music_list[rand()%music_list.size()]))
            return -1; // erreur

        music.setLoop(true);
        music.play();
        arret=scene1.Village(window,&perso1,salle, pack);
        music.stop();
        music.setLoop(false);
        if(arret)
            return EXIT_SUCCESS;
    }
    Sleep(3000);

    window.setVisible(false);

    afficher_ennemi_c(".\\autre\\mechant.pgm.txt");
    afficher_ennemi_c(".\\autre\\notover.pgm.txt");

    Sleep(3000);
    window.setVisible(true);
    window.create(sf::VideoMode(1600, 1200), "Abscond-the game",sf::Style::Fullscreen);
    window.draw(fond);
    window.display();

    Sleep(3000);


    nbmap=rand()%3+3;
    for (int k=0;k<nbmap;k++)
    {
        pack=1+rand()%4;
        vector<string> music_list;
        s_rep.str("");
        s_rep << ".\\music\\bugged\\pack"<<pack<<"\\";
        rep = opendir(s_rep.str().c_str());
        while ((lecture = readdir(rep))){
            nom = (lecture->d_name);
            int i = nom.size();
            if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".wav")!=0)){}
            else{
                nom = nom.substr(0,i-4);
                s_rep.str("");
                s_rep << ".\\music\\bugged\\pack"<<pack<<"\\"<<nom<<".wav";
            music_list.push_back(s_rep.str().c_str());}
        }
        closedir(rep);
        if (!music.openFromFile(music_list[rand()%music_list.size()]))
            return -1; // erreur
        music.setLoop(true);
        music.play();
        arret=scene1.Village2(window,&perso1,salle,(k+1), pack);
        music.stop();
        music.setLoop(false);
        if(arret)
            return EXIT_SUCCESS;

    }

    view1.setCenter(sf::Vector2f(450,300));
    view1.setSize(sf::Vector2f(900,600));
    window.setView(view1);
    if (!textfond.loadFromFile(".\\image\\BlueScreen1.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();

    Sleep(500);

    if (!textfond.loadFromFile(".\\image\\BlueScreen2.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();

    Sleep(500);

    if (!textfond.loadFromFile(".\\image\\BlueScreen3.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();

    Sleep(500);

    if (!textfond.loadFromFile(".\\image\\BlueScreen4.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    char s[4];
    for (int k=0; k<21; k++)
    {
        sprintf(s,"%d",k*5);
        window.draw(fond);
        // Declare and load a font
        sf::Font font;
        font.loadFromFile("arial.ttf");
        // Create a text
        sf::Text text(s, font);
        text.setCharacterSize(16);
        text.setColor(sf::Color::White);
        text.setPosition(280,468);
        window.draw(text);
        // Draw it
        window.display();
        Sleep(50);
    }


    if (!textfond.loadFromFile(".\\image\\BlueScreen5.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();

    pack=1+rand()%4;
    vector<string> music_list;
    s_rep.str("");
    s_rep << ".\\music\\boss\\";
    rep = opendir(s_rep.str().c_str());
    while ((lecture = readdir(rep))){
        nom = (lecture->d_name);
        int i = nom.size();
        if (i<4 || (strcmp((nom.substr(i-4,i-1)).c_str(),".wav")!=0)){}
        else{
            nom = nom.substr(0,i-4);
            s_rep.str("");
            s_rep << ".\\music\\boss\\"<<nom<<".wav";
        music_list.push_back(s_rep.str().c_str());}
    }
    closedir(rep);
    if (!music.openFromFile(music_list[rand()%music_list.size()]))
        return -1; // erreur
    music.setLoop(true);
    music.play();
    scene1.Village2(window,&perso1,14,0, pack);
    music.stop();
    music.setLoop(false);

    view1.setCenter(sf::Vector2f(800,600));
    view1.setSize(sf::Vector2f(1600,1200));
    window.setView(view1);

    if (!textfond.loadFromFile(".\\image\\FondNoir.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,1600,1200));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();
    Sleep(500);
    if (!textfond.loadFromFile(".\\image\\GodMode1.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,738,311));
    textfond.setRepeated(true);
    fond.setPosition((1600-738)/2,(1200-311)/2);
    window.draw(fond);
    window.display();
    Sleep(1500);
    if (!textfond.loadFromFile(".\\image\\FondNoir.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition((1600-900)/2,(1200-600)/2);
    window.draw(fond);
    window.display();

    if (!textfond.loadFromFile(".\\image\\GodMode2.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,737,309));
    textfond.setRepeated(true);
    fond.setPosition((1600-737)/2,(1200-309)/2);
    window.draw(fond);
    window.display();
    Sleep(1500);

    if (!textfond.loadFromFile(".\\image\\FondNoir.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,1600,1200));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();

    if (!music.openFromFile(".\\Song\\cogne.wav"))
        return -1; // erreur


    if (!textfond.loadFromFile(".\\image\\crack1.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition((1600-900)/2,(1200-600)/2);
    window.draw(fond);
    window.display();
    music.play();
    Sleep(1500);

    if (!textfond.loadFromFile(".\\image\\crack2.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition((1600-900)/2,(1200-600)/2);
    window.draw(fond);
    window.display();
    music.play();
    Sleep(1500);

    if (!textfond.loadFromFile(".\\image\\crack3.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,900,600));
    textfond.setRepeated(true);
    fond.setPosition((1600-900)/2,(1200-600)/2);
    window.draw(fond);
    window.display();
    music.play();
    Sleep(1500);

    if (!music.openFromFile(".\\Song\\Avast.wav"))
        return -1; // erreur
    music.play();

    view1.setCenter(sf::Vector2f(800,600));
    view1.setSize(sf::Vector2f(1600,1200));
    window.setView(view1);
    if (!textfond.loadFromFile(".\\image\\FondNoir.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,1600,1200));
    textfond.setRepeated(true);
    fond.setPosition(0,0);
    window.draw(fond);
    window.display();


    Sleep(3000);

    if (!textfond.loadFromFile(".\\image\\avast.png")){}

    fond.setTexture(textfond,true);
    fond.setTextureRect(sf::IntRect(0,0,450,300));
    textfond.setRepeated(true);
    fond.setPosition(1600-450,1200-300);
    window.draw(fond);
    window.display();
    Sleep(1000);
    music.stop();
    Sleep(2000);

    return EXIT_SUCCESS;
}
