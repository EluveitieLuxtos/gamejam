Le but de ce jeu est de fuir les ennemis, sans pouvoir les attaquer.

Ce jeu est écrit en C++ en utilisant la bibliothèque SFML. Il se joue niquement aux flèches directionnelles du clavier.

Le joueur est au départ placé dans une salle dont il doit s'échapper.

Au bout de quelques niveaux, l'IA du jeu ne supporte plus que vous vous en sortiez !

Vous possédez au départ au minimum 5 vies. Le maximum est... à vous de le découvrir.

Les musiques sont tirées du site : retro.sx/music
Bully                -> Ville
Dragon Quest 4       -> Cave
Mario Kart Arcade GP -> Forêt
Micro Machines 2     -> Boss
Pac-Man              -> Nuages

Réalisé par Luxtos, Neygon, Dr3m
