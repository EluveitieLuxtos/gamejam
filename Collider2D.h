#ifndef Collider2Dc_H
#define Collider2Dc_H

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <math.h>


#define PI 3.14159265359

using namespace std;

class Collider2Dc
{
    public:
        Collider2Dc();
        virtual ~Collider2Dc();
        void init_col(int x, int y, int w, int h);
        bool coll_collide(Collider2Dc coll2);
        bool coll_groupe(vector<Collider2Dc> gc2d);
        void coll_rotate(double ang);
        void chg_pos(int x, int y);
        void chg_taille(int w, int h);
        double coll_rotation();
        int positionx();
        int positiony();
        int taillew();
        int tailleh();
    protected:
    private:
        int posx,posy,height,width;
        double angle;
};

#endif // Collider2Dc_H
